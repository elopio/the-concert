# [Canciones de Schubert, con y sin palabras](https://www.gardnermuseum.org/audio/player/4498)

## Descripción

* [Schubert](https://es.wikipedia.org/wiki/Franz_Schubert):
  [Impromptu en sol bemol mayor para piano, D. 899/3, Op. 90, No. 3](https://en.wikipedia.org/wiki/Impromptus_(Schubert)#No._3_in_G-flat_major)
  (14 de octubre, 2001)
* [Schubert](https://es.wikipedia.org/wiki/Franz_Schubert):
  [Winterreise («Viaje de invierno), D. 911, Op. 89, Parte I](https://es.wikipedia.org/wiki/Viaje_de_invierno)
  (19 de enero, 2003)

El programa de esta semana se enfoca de nuevo en Schubert, y su don para
melodías que cantan. En la primera pieza, la melodía lírica en la mano
derecha del pianista es una tonada que podría ser fácilmente la línea vocal de
una de las canciones de Schubert. La mano derecha acompaña con devoción la
tonada, brindando soporte armónico y movimiento rítmico. Esta idea, de una
línea melódica soportada por un acompañamiento evocativo en piano, figura
prominentemente en las canciones de Schubert, incluyendo la otra pieza del
programa: Winterreise. Este ciclo de canciones fue escrito tarde en la vida
de Schubert, durante una enfermedad seria, y el narrador en las canciones
contempla y confronta la muerte por todas partes. En un particular momento
conmovedor a mitad de esste extracto, en la canción «Dier Lindenbaum», el
narrador encuentra un breve respiro bajo las ramas de un árbol de tilo. Pero,
como en gran parte de la canción de Schubert, tal vez hay una correlación algo
inquietante entre descanso y muerte. Los poemas cuentan la historia de un viaje
de invierno a través de los bosques fríos y nevados. Pueden encontrar las
traducciones completas en
[lieder.net](http://www.lieder.net/lieder/assemble_texts.html?SongCycleId=47),
para que puedan seguir la historia. Asegúrense de revisar de vuelta el
podcast #6, y la conclusión de Winterreise.
