# The Concert

You can listen to all the music performed at the Isabella Stewart Gardner Museum
at https://www.gardnermuseum.org/experience/music
([RSS feed](https://www.gardnermuseum.org/rss.xml)).

These are translations of their music podcast.

# Presentation

## English/

Hello, and welcome to The Concert. I'm Scott Nickrenz, music director at the
Isabella Stewart Gardner Museum in Boston, and for the next 45 minutes we'll be
listening to some of the most outstanding live performances heard here at the
museum. If you like what you hear, you can find new, free, classical music
posted every two weeks and subscribe to the podcast at our website at
[gardnermuseum.org](https://www.gardnermuseum.org). For now though, I invite
you into the museum's tapestry room for some great performances you won't hear
anywhere else.

## Español/

Hola, y bienvenidas a El concierto. Yo soy Scott Nickrenz, director musical en
el Museo Isabella Stewart Gardner en Boston, y por los siguientes 45 minutos
escucharemos algunas de las más excepcionales interpretaciones en vivo que se
han oído aquí en el museo. Si les gusta lo que oyen, pueden encontrar música
clásica nueva y gratuita publicada cada dos semanas y suscribirse al podcast en
nuestro sitio web en [gardnermuseum.org](https://www.gardnermuseum.org). Pero
por ahora, les invito a la sala de tapices del museo para algunas de las
mejores interpretaciones que no oirán en ningún otro lugar.

# Conclusion

## English/

Thanks for listening to The Concert. You will find more information of the
music you've heard on our website at
[gardnermuseum.org](https://www.gardnermuseum.org). You can also subscribe to
receive automatic updates every two weeks when a new podcast is released. On
our website you'll also find information schedules of our upcoming concerts
where you can here more great live music almost every Sunday from September
throug May here at the Isabella Stewart Gardner Museum.


1. [Beethoven: Before and After](https://www.gardnermuseum.org/audio/player/4501):
   * [English](1-beethoven-before-and-after.en.md) / [Español](1-beethoven-before-and-after.es.md)

2. [Mozart Mini-Marathon](https://www.gardnermuseum.org/audio/player/4500):
   * [English](2-mozart-mini-marathon.en.md) / [Español](2-mozart-mini-marathon.es.md)

3. [Schubert: Inspired by Song](https://www.gardnermuseum.org/audio/player/4499):
   * [English](3-schubert-inspired-by-song.en.md) / [Español](3-schubert-inspired-by-song.es.md)

4. [Schubert's Songs, With and Without Words](https://www.gardnermuseum.org/audio/player/4498):
   * [English](4-schuberts-songs-with-and-without-words.en.md) / [Español](4-schuberts-songs-with-and-without-words.es.md)

5. [Chopin's Piano Fireworks](https://www.gardnermuseum.org/audio/player/4497):
   * [English](5-chopins-piano-fireworks.en.md) / [Español](5-chopins-piano-fireworks.es.md)