# [Schubert: Inspired by Song](https://www.gardnermuseum.org/audio/player/4499)

## Description

* [Schubert](https://en.wikipedia.org/wiki/Franz_Schubert):
  [Der Hirt auf dem Felsen (“The Shepherd on the Rock”) for voice, clarinet, and piano, D. 965, Op. 129](https://en.wikipedia.org/wiki/The_Shepherd_on_the_Rock)
  (March 6, 2006)
* [Schubert](https://en.wikipedia.org/wiki/Franz_Schubert):
  [String Quartet No. 14 in D minor, D. 810 (“Death and the Maiden”)](https://en.wikipedia.org/wiki/String_Quartet_No._14_(Schubert))
  (May 1, 2005)

This week’s program features two chamber music pieces, one with voice and one
without, both written late in Schubert’s life, and both inspired by his love of
song. “The Shepherd on the Rock” is longer than most of Schubert’s 600 songs,
at about fifteen minutes, and in many ways is more like a chamber music piece
than a song. The narrator, a shepherd singing of his far away beloved, moves
from wistfulness to despair to hope. In the final section, as the narrator
sings of faith in the coming springtime, the clarinet and voice echo each
other’s ascending lines, acting as chamber music partners rather than as
soloist and accompanist. The string quartet “Death and the Maiden,” uses a
song as inspiration for an entirely instrumental work. The second movement of
this quartet is a set of variations on the theme from Schubert’s song “Death
and the Maiden.” By using the melody of the song, he evokes its story, too. In
“Death and the Maiden,” a young woman pleads with the personified “death” to
spare her life, but as death seductively promises rest and peace he seems to
calm her fears, perhaps luring her away. The use of this melody perhaps also
reflects Schubert’s own confrontation with death. As he was writing the
quartet, he was hospitalized and in poor health. He died only a few years
later, at age 31. In 1824, he wrote to a friend, “Each night, when I go to
sleep, I hope I will not wake again.”
