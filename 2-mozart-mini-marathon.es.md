# [Mini maratón de Mozart](https://www.gardnermuseum.org/audio/player/4500)

## Descripción

* [Mozart](https://es.wikipedia.org/wiki/Wolfgang_Amadeus_Mozart):
  [Sonata para violín No. 21, K. 305](https://es.wikipedia.org/wiki/Sonata_para_viol%C3%ADn_n.%C2%BA_21_(Mozart))
  (29 de enero, 2006)
* [Mozart](https://es.wikipedia.org/wiki/Wolfgang_Amadeus_Mozart):
  [Sinfonía concertante para violín y viola, K. 364](https://es.wikipedia.org/wiki/Sinfon%C3%ADa_concertante_para_viol%C3%ADn,_viola_y_orquesta_(Mozart)):
  (29 de enero, 2006)

Únansenos para dos interpretaciones que celebran el 250 aniversario del más
notorio prodigio de la música, Wolfgang Amadeus Mozart. Estos conciertos,
grabados en vivo durante nuestra Maratón de Mozart en enero de 2006, presentan
algunos de nuestros solistas favoritos así como la Orquesta de cámara Gardner,
el ensamble residente del museo. Para iniciar, el violinista
[Corey Cerovsek](https://en.wikipedia.org/wiki/Corey_Cerovsek) y
el pianista
[Jeremy Denk](https://es.wikipedia.org/wiki/Jeremy_Denk) interpretan la
encantadora Sonata para violín en mi menor. Luego, a Corey se le une la
violista [Kim Kashkashian](https://es.wikipedia.org/wiki/Kim_Kashkashian) y la
Orquesta de cámara Gardner para una aradiente rendición de la confrontación de
violín contra viola de Mozart, su Sinfonía Concertante. Mozart era violista,
así como nuestro Director musical (y violista) Scott Nickrenz. Tal vez cuando
escribió esta pieza él estaba tratando de resolver la antigual rivalidad entre
violistas y violinistas. Mozart reta los límites técnicos de ambos instrumentos
y hace la pregunta: ¿puede una violista seguirle el paso a una violinista?
¿Puede una violista seguirle el paso a una violinista?
