# [Chopin's Piano Fireworks](https://www.gardnermuseum.org/audio/player/4497)

## Descripción

* [Chopin](https://es.wikipedia.org/wiki/Fr%C3%A9d%C3%A9ric_Chopin): [Doce estudios, Op. 10](https://es.wikipedia.org/wiki/Estudios_(Chopin)#Estudios_Op._10)

Cuando uno escucha los estudios de Chopin, puede notar que él mismo era un
pianista virtuoso, e íntimamente familiar con el piano. Los estudios son piezas
cortas pero retadoras con el objetivo de estirar los límites técnicos de las
pianistas y desarrollar su técnica. Pero los estudios de Chopin retaban no sólo
su propia habilidad de interpretarlas, sino también su ingenuidad para la
composición. Chopin escribió el primero de estos estudios cuándo tenía sólo 19
años, y estos fueron publicados cuando tenía sólo 23. Escritos para los salones
parisinos en donde Chopan interpretaba y socializaba, estas piezas se sienten
como en casa en el ambiente del Museo Gardner, donde Isabella Gardner
organizaba veladas músicales íntimas y entretenía a eminentes artistas y
pensadoras. Hoy, este legado de mecenazgo, conexión activa con arte y
artistas, y discurso sobre las artes continúa, con artistas residentes
trabajando en el museo y músicas llenando el museo con música cada domingo.
Como los salones de París, este animado escenario artístico es el tipo de lugar
en el que uno podría haber oído por primera vez estos estudios allá en 1833.
