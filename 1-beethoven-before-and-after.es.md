# [Beethoven: antes y después](https://www.gardnermuseum.org/audio/player/4501):

## Descripción

* [Beethoven](https://es.wikipedia.org/wiki/Ludwig_van_Beethoven):
  [Sonata para violín No. 3 en mi bemol mayor, Op. 12, No. 3](https://en.wikipedia.org/wiki/Violin_Sonata_No._3_(Beethoven))
  (14 de marzo   2004)
* [Beethoven](https://en.wikipedia.org/wiki/Ludwig_van_Beethoven):
  [Trío para piano en re mayor, Op. 70, No. 1 (“Ghost”)](https://en.wikipedia.org/wiki/Piano_Trios,_Op._70_(Beethoven)#Piano_Trio_in_D_major,_Op.70_No.1_%22Ghost%22)
  (19 de marzo, 2006)

En este programa escuchamos dos piezas de Beethoven para cuerdas y piano: una
melodiosa y temprana sonata para violín y el famoso «Trío fantasma», escrito 12
años después. Beethoven escribió la sonata cuando estaba viviendo en Viena,
y el «clasicismo vienés» personificado en la música de Haydn y Mozart era el
último grito. En ese momento, los trabajos tempranos de Beethoven estaban
siendo encontrados con éxito y entusiasmo, y él estába de gira por Europa como
pianista. Más de una década después, cuando Beethoven escribió el «Trío
fantasma» en 1809, él estaba perdiendo rapidamente su audición y sabía que la
degeneración que causaría su eventual sordera probablemente era intratable. A
pesar de esto, o tal vez incluso debido a esto, este profundo cambio en la
forma en que escuchaba el sonido, su música mostró una innovación increíble. En
el «Trío fantasma», así llamado por el cromatismo con sonido espeluznante en el
segundo movimiento, el piano se convierte en una pareja igualitaria de los
instrumentos de cuerdas, y se reusan de forma creativa fragmentos de material
musical, en lugar de ser repetidos de forma literal. Este dramático cambio de
imagen musical muestra el crecimiento de Beethoven como compositor a través del
lente de sus trabajos para cuerdas y piano.
