# [Beethoven: Before and After](https://www.gardnermuseum.org/audio/player/4501):

## Description

* [Beethoven](https://en.wikipedia.org/wiki/Ludwig_van_Beethoven):
  [Violin Sonata No. 3 in E-flat Major, Op. 12, No. 3](https://en.wikipedia.org/wiki/Violin_Sonata_No._3_(Beethoven))
  (March 14,  2004)
* [Beethoven](https://en.wikipedia.org/wiki/Ludwig_van_Beethoven):
  [Piano Trio in D Major, Op. 70, No. 1 (“Ghost”)](https://en.wikipedia.org/wiki/Piano_Trios,_Op._70_(Beethoven)#Piano_Trio_in_D_major,_Op.70_No.1_%22Ghost%22)
  (March 19, 2006)

In this program, we hear two Beethoven pieces for strings and piano: a tuneful
early violin sonata and the famous “Ghost Trio,” written 12 years later.
Beethoven wrote the sonata when he was living in Vienna, and “Viennese
Classicism,” epitomized in the music of Haydn and Mozart, was all the rage. At
the time, Beethoven’s early works were being met with success and enthusiasm,
and he was touring Europe as a pianist. More than a decade later, as Beethoven
wrote the “Ghost Trio” in 1809, he was rapidly losing his hearing, and he knew
that the degeneration that would cause his eventual deafness was probably
untreatable. In spite of, or perhaps even because of, this profound change in
the way he heard sound, his music showed incredible innovation. In the “Ghost
Trio,” so named for the spooky-sounding chromaticism in the second movement,
the piano becomes an equal partner of the string instruments, and snippets of
musical material are reused creatively, rather than repeated verbatim. This
dramatic musical makeover shows Beethoven’s growth as a composer through the
lens of his works for strings and piano.
