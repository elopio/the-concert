# [Chopin's Piano Fireworks](https://www.gardnermuseum.org/audio/player/4497)

## Description

* [Chopin](https://en.wikipedia.org/wiki/Fr%C3%A9d%C3%A9ric_Chopin): [Twelve Etudes, Op. 10](https://en.wikipedia.org/wiki/%C3%89tudes_(Chopin)#%C3%89tudes_Op._10) (September 22, 2002)

When you hear Chopin’s etudes, you can tell that he was a virtuosic pianist
himself, and intimately familiar with the piano. Etudes are short but
challenging studies meant to stretch the pianist’s technical boundaries and
develop his technique. But Chopin’s etudes challenged not only his own playing
ability, but also his compositional ingenuity. Chopin wrote the first of these
etudes when he was only 19 years old, and they were published when he was just
23. Written for the Parisian salons where Chopin played and socialized, these
pieces are quite at home in the environment of the Gardner Museum, where
Isabella Gardner hosted intimate musical soirees and entertained eminent
artists and thinkers. Today, this legacy of patronage, active connection with
art and artists, and discourse about the arts continues, with
Artists-in-Residence working in the museum and musicians filling the museum
with music every Sunday. Like the salons of Paris, this lively artistic
setting is the sort of place one might have first heard these etudes back in
1833.
