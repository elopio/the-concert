# [Mozart Mini-Marathon](https://www.gardnermuseum.org/audio/player/4500)

## Description

* [Mozart](https://en.wikipedia.org/wiki/Wolfgang_Amadeus_Mozart):
  [Violin Sonata No. 21, K. 305](https://en.wikipedia.org/wiki/Violin_Sonata_No._21_(Mozart))
  (January 29, 2006)
* [Mozart](https://en.wikipedia.org/wiki/Wolfgang_Amadeus_Mozart):
  [Sinfonia Concertante for violin and viola, K. 364](https://en.wikipedia.org/wiki/Sinfonia_Concertante_for_Violin,_Viola_and_Orchestra_(Mozart))
  (January 29, 2006)

Join us for two performances celebrating the 250th birthday of music’s most
notorious prodigy, Wolfgang Amadeus Mozart. These concerts, recorded live
during our Mozart Marathon in January of 2006, feature some of our favorite
soloists as well as the Gardner Chamber Orchestra, the museum’s resident
ensemble. To start, violinist
[Corey Cerovsek](https://en.wikipedia.org/wiki/Corey_Cerovsek) and pianist
[Jeremy Denk](https://en.wikipedia.org/wiki/Jeremy_Denk) perform Mozart’s
delightful violin sonata in E minor. Then, Corey is joined by violist
[Kim Kashkashian](https://en.wikipedia.org/wiki/Kim_Kashkashian) and the
Gardner Chamber Orchestra for a fiery rendition of Mozart’s violin-versus-viola
showdown, his Sinfonia Concertante. Mozart himself was a violist, like our
Music Director (and violist) Scott Nickrenz. Perhaps when he wrote this piece
he was trying to settle the age-old rivalry between violists and violinists.
Mozart challenges the technical boundaries of both instruments and asks the
question: can a violist keep up with a violinist? Can a violist keep up with
a violinist?
