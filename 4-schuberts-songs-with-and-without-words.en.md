# [Schubert's Songs, With and Without Words](https://www.gardnermuseum.org/audio/player/4498)

## Description

* [Schubert](https://en.wikipedia.org/wiki/Franz_Schubert):
  [Impromptu in G-flat Major for piano, D. 899/3, Op. 90, No. 3](https://en.wikipedia.org/wiki/Impromptus_(Schubert)#No._3_in_G-flat_major)
  (October 14, 2001)
* [Schubert](https://en.wikipedia.org/wiki/Franz_Schubert):
  [Winterreise (“Winter Journey”), D. 911, Op. 89, Part I](https://en.wikipedia.org/wiki/Winterreise)
  (January 19, 2003)

This week’s program focuses again on Schubert, and his gift for a singing
melody. In the first piece, the lyrical melody in the pianist’s right hand is a
tune that could easily be the vocal line of one of Schubert’s songs. The left
hand devotedly accompanies the tune, providing harmonic support and rhythmic
motion. This idea, of a melodic line supported by an evocative piano
accompaniment, figures prominently in Schubert’s songs, including the other
piece on the program: Winterreise. This song cycle was written late in
Schubert’s life, during a serious illness, and the narrator in the songs
contemplates and confronts death throughout. In a particularly poignant moment
halfway through this excerpt, in the song “Der Lindenbaum,” the narrator finds
a brief respite under the branches of a Linden tree. But, as in much of
Schubert’s song, there is perhaps a somewhat unsettling correlation between
rest and death. The poems tell the story of a winter’s journey through the cold
and snowy woods. You can find the complete translations at
[lieder.net](http://www.lieder.net/lieder/assemble_texts.html?SongCycleId=47),
so you can follow the story. Be sure to check back for podcast #6, and the
conclusion of Winterreise.
