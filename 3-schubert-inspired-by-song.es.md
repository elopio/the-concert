# [Schubert: inspirado por una canción](https://www.gardnermuseum.org/audio/player/4499)

## Descripción

* [Schubert](https://es.wikipedia.org/wiki/Franz_Schubert):
  [Der Hirt auf dem Felsen («El pastor en la roca») para voz, clarinete y piano, D. 965, Op. 129](https://es.wikipedia.org/wiki/El_pastor_en_la_roca)
  (6 de marzo, 2006)
* [Schubert](https://es.wikipedia.org/wiki/Franz_Schubert):
  [String Quartet No. 14 en Re menor, D. 810 («La muerte y la doncella»)](https://es.wikipedia.org/wiki/Cuarteto_de_cuerda_n.%C2%BA_14_(Schubert))
  (1 de mayo, 2005)

El programa de esta semana presenta dos piezas de música de cámara, una con voz
y otra sin, ambas escritas tarde en la vida de Schubert, y ambas inspiradas por
su amor por la canción. «El pastor en la roca» es más larga que la mayoría de
las 600 canciones de Schubert, con cerca de quince minutos, y en muchas formas
es más como una pieza de música de cámara que una canción. El narrador, un
pastor cantando sobre su alejada amada, se mueve desde un anhelo triste hacia
la desesperación hacia la esperanza. En la sección final, cuando el narrador
canta sobre la fe en la primavera venidera, el clarinete y la voz se hacen eco
a las líneas ascendentes del otro, actuando como compañeros de música de cámara
en lugar de como solista y acompañante. El cuarteto de cuerdas «La muerte y la
doncella» usa una canción como inspiración para un trabajo enteramente
instrumental. El segundo movimiento de este cuarteto es un conjunto de
variaciones sobre el tema de la canción de Schubert «La muerte y la doncella».
Al usar la melodía de la canción, él evoca su historia, también. En «La muerte
y la doncella», una joven mujer implora a la «muerte» personificada que le
perdone la vida, pero conforme la muerte seductoramente le promete descanso y
paz él parece calmar sus miedos, talvez atrayéndola. El uso de esta meldía tal
vez también reflexiona sobra la propia confrontación de Schubert con la muerte.
Cuando estaba escribiendo el cuarteto, él estaba hospitalizado y con pobre
salud. Él murió sólo unos pocos años después, a la edad de 31. En 1824, él
escribió a una amistad, «Cada noche, cuando voy a dormir, tengo la esperanza de
no despertar de nuevo».
